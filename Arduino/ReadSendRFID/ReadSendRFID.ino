#include <SoftwareSerial.h>
String s="";
int i=0;
SoftwareSerial rfidSerial(10, 11); // RX, TX
void setup() 
{
  // put your setup code here, to run once:
  rfidSerial.begin(9600); //inicjalizacja portu serial, do którego podłączony jest moduł RFID
  Serial.begin(9600); //inicjalizacja portu serial do komunikacji z komputerem
}

void loop() 
{
  // put your main code here, to run repeatedly:
  if (rfidSerial.available() > 0) //jeśli czujnik coś zczytał
  { 
    i = rfidSerial.read();  //zczytanie jednego fragmentu znacznika
    s = s + String(i);  //budowa łańcucha znakowego z fragmentów znacznika
    Serial.print(i, DEC);  //wypisanie na monitor szeregowy znacznika
    Serial.print(" ");  //odzielanie poszczególnych fragmentów znacznika
    if (i == 3) //warunek sprawdzający czy zczytaliśmy cały znacznik
    {  
      Serial.print("\n");  //przejście do nowej linii
    }
  }
}
